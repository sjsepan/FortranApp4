!FTN95/ClearWin+ application...
WINAPP 'CWP_ICO.RC'
PROGRAM main
	IMPLICIT NONE
    INCLUDE <windows.ins>
    EXTERNAL filenew, fileopen, filesave, filesaveas, fileprint
    EXTERNAL editundo, editredo, editselectall, editcut, editcopy, editpaste, editdelete, editfind, editreplace
    EXTERNAL editrefresh, editpreferences
    EXTERNAL windownewwindow, windowtile, windowcascade, windowarrangeall, windowhide, windowshow
    EXTERNAL helpcontents, helpindex, helponlinehelp, helplicenceinformation, helpcheckforupdates, helpabout
	INTEGER ans
    DOUBLE PRECISION d
!    CHARACTER*129 file,new_file
	CHARACTER*50 status
	COMMON status
    d=0.33
    status=''
	ans=winio@('%ca[FortranApp4]&')
    ans=winio@('%ww%pv&')
    ans=winio@('%mn[&File[&New]]&',filenew)
    ans=winio@('%mn[[&Open...]]&',fileopen)
    ans=winio@('%mn[[&Save]]&',filesave)
    ans=winio@('%mn[[Save &As...]]&',filesaveas)
    ans=winio@('%mn[[|,&Print...]]&',fileprint)
	ans=winio@('%mn[[|,E&xit]]&','EXIT')
	ans=winio@('%mn[&Edit[&Undo]]&',editundo)
	ans=winio@('%mn[[&Redo]]&',editredo)
	ans=winio@('%mn[[|,&Select All]]&',editselectall)
	ans=winio@('%mn[[&Cut]]&',editcut)
	ans=winio@('%mn[[&Copy]]&',editcopy)
	ans=winio@('%mn[[&Paste]]&',editpaste)
	ans=winio@('%mn[[&Delete]]&',editdelete)
	ans=winio@('%mn[[|,&Find]]&',editfind)
	ans=winio@('%mn[[&Replace]]&',editreplace)
	ans=winio@('%mn[[|,&Refresh]]&',editrefresh)
	ans=winio@('%mn[[|,&Preferences]]&',editpreferences)
	ans=winio@('%mn[&Window[&New Window]]&',windownewwindow)
	ans=winio@('%mn[[&Tile]]&',windowtile)
	ans=winio@('%mn[[&Cascade]]&',windowcascade)
	ans=winio@('%mn[[&Arrange All]]&',windowarrangeall)
	ans=winio@('%mn[[|,&Hide]]&',windowhide)
	ans=winio@('%mn[[&Show]]&',windowshow)
	ans=winio@('%mn[&Help[&Contents]]&',helpcontents)
	ans=winio@('%mn[[&Index]]&',helpindex)
	ans=winio@('%mn[[&Online Help]]&',helponlinehelp)
	ans=winio@('%mn[[|,&Licence Information]]&',helplicenceinformation)
	ans=winio@('%mn[[&Check For Updates]]&',helpcheckforupdates)
	ans=winio@('%mn[[|,&About...]]&',helpabout)
    !define a 30x10 edit box %eb
    ans=winio@('%30.10eb[vscrollbar,hscrollbar]&','*',0)
    ans=winio@('%14nl%ob%42st%cb&',status)!use 14 because box is 10 high
    ans=winio@('%20br&',d,RGB@(0,255,0))
    ans=winio@('%ac[Ctrl+N]&',filenew)
    ans=winio@('%ac[Ctrl+O]&',fileopen)
    ans=winio@('%ac[Ctrl+S]&',filesave)
    ans=winio@('%ac[Ctrl+P]&',fileprint)
    ans=winio@('%ac[Ctrl+Q]&','EXIT')
    ans=winio@('%ac[Ctrl+Z]&',editundo)
    ans=winio@('%ac[Ctrl+Y]&',editredo)
    ans=winio@('%ac[Ctrl+A]&',editselectall)
    ans=winio@('%ac[Ctrl+X]&',editcut)
    ans=winio@('%ac[Ctrl+C]&',editcopy)
    ans=winio@('%ac[Ctrl+V]&',editpaste)
    ans=winio@('%ac[Del]&',editdelete)
    ans=winio@('%ac[Ctrl+F]&',editfind)
    ans=winio@('%ac[Ctrl+H]&',editreplace)
    ans=winio@('%ac[F5]&',editrefresh)
    ans=winio@('%ac[F1]',helpcontents)
END PROGRAM main

INTEGER FUNCTION filenew()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='New'
	CALL window_update@(status)
    filenew=2
END

INTEGER FUNCTION fileopen()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Open'
	CALL window_update@(status)
    fileopen=2
END

INTEGER FUNCTION filesave()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Save'
	CALL window_update@(status)
    filesave=2
END

INTEGER FUNCTION filesaveas()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Save As'
	CALL window_update@(status)
    filesaveas=2
END

INTEGER FUNCTION fileprint()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Print'
	CALL window_update@(status)
    fileprint=2
END

INTEGER FUNCTION editundo()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Undo'
	CALL window_update@(status)
    editundo=2
END

INTEGER FUNCTION editredo()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Redo'
	CALL window_update@(status)
    editredo=2
END

INTEGER FUNCTION editselectall()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Select All'
	CALL window_update@(status)
    editselectall=2
END

INTEGER FUNCTION editcut()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Cut'
	CALL window_update@(status)
    editcut=2
END

INTEGER FUNCTION editcopy()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Copy'
	CALL window_update@(status)
    editcopy=2
END

INTEGER FUNCTION editpaste()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Paste'
	CALL window_update@(status)
    editpaste=2
END

INTEGER FUNCTION editdelete()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Delete'
	CALL window_update@(status)
    editdelete=2
END

INTEGER FUNCTION editfind()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Find'
	CALL window_update@(status)
    editfind=2
END

INTEGER FUNCTION editreplace()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Replace'
	CALL window_update@(status)
    editreplace=2
END

INTEGER FUNCTION editrefresh()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Refresh'
	CALL window_update@(status)
    editrefresh=2
END

INTEGER FUNCTION editpreferences()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Preferences'
	CALL window_update@(status)
    editpreferences=2
END

INTEGER FUNCTION windownewwindow()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='New Window'
	CALL window_update@(status)
    windownewwindow=2
END

INTEGER FUNCTION windowtile()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Tile'
	CALL window_update@(status)
    windowtile=2
END

INTEGER FUNCTION windowcascade()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Cascade'
	CALL window_update@(status)
    windowcascade=2
END

INTEGER FUNCTION windowarrangeall()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Arrange All'
	CALL window_update@(status)
    windowarrangeall=2
END

INTEGER FUNCTION windowhide()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Hide'
	CALL window_update@(status)
    windowhide=2
END

INTEGER FUNCTION windowshow()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Show'
	CALL window_update@(status)
    windowshow=2
END

INTEGER FUNCTION helpcontents()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Contents'
	CALL window_update@(status)
    helpcontents=2
END

INTEGER FUNCTION helpindex()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Index'
	CALL window_update@(status)
    helpindex=2
END

INTEGER FUNCTION helponlinehelp()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Online Help'
	CALL window_update@(status)
    helponlinehelp=2
END

INTEGER FUNCTION helplicenceinformation()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Licence Information'
	CALL window_update@(status)
    helplicenceinformation=2
END

INTEGER FUNCTION helpcheckforupdates()
	IMPLICIT NONE
	CHARACTER*50 status
	COMMON status
    status='Check For Updates'
	CALL window_update@(status)
    helpcheckforupdates=2
END

INTEGER FUNCTION helpabout()
	IMPLICIT NONE
	INCLUDE <windows.ins>
	INTEGER ans
	ans=winio@('%ca[About FortranApp4]&')
	ans=winio@('%fn[Times New Roman]%ts%bf%cnFortranApp4&',2.0D0)
	ans=winio@('%ts%4nl&',1.0D0)
	ans=winio@('%cnFortran95 / ClearWin+ GUI prototype%2nl&')
	ans=winio@('%tc%sf%2nl%cnby&',-1)
	ans=winio@('%2nl%cnStephen J Sepan&')
	ans=winio@('%2nl%cn%9`bt[OK]')
	helpabout=2
END
