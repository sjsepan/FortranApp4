# readme for FortranApp4 v0.1

## About

Desktop GUI app prototype, on Windows, in Fortran95, using ClearWin+

![FortranApp4.png](./FortranApp4.png?raw=true "Screenshot")

### Notes

~Created a basic menu, but have shelved the project. Designing a form with ClearWin+ format codes works but I found it tedious.
~Adding icons to menu items requires using an alternate format code which requires that the text be part of the image.

### Documentation

~SilverFrost Fortran95: <https://www.silverfrost.com/11/ftn95_overview.aspx>
~ClearWin+: <https://www.silverfrost.com/18/ftn95/clearwin.aspx>
~ClearWin+ Manual: <https://www.silverfrost.com/manuals/clearwin.pdf>

### VSCode

Primary coding was done in Plato; VSCode was used for documenting and preparing the project for posting.

### Issues

~

### History

0.1:
~initial release

Steve Sepan
ssepanus@yahoo.com
10/29/2022
